package com.choucair.formacion.utilities;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBManager {

	private static DBManager instance;
	private String url;
	private String user;
	private String password;

	/** Creates a new instance of DBManager */
	private DBManager() {
		inicialice();
	}

	public void inicialice() {
		try {
			Properties prop = new Properties();
			prop.load(new FileReader("/home/cesareots/eclipse-workspace-0/formacion/dbconfig.properties"));
			this.url = prop.getProperty("db.url");
			this.user = prop.getProperty("db.user");
			this.password = prop.getProperty("db.password");
			Class.forName(prop.getProperty("db.driver"));
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}

	// aplicando Singleton
	public static DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}

	public Connection getConeccion() throws SQLException {
		Connection con = DriverManager.getConnection(this.url, this.user, this.password);
		return con;
	}
}
