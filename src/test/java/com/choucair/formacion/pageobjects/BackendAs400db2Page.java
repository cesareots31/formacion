package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.choucair.formacion.utilities.Sql_Execute;

import net.serenitybdd.core.pages.PageObject;

public class BackendAs400db2Page extends PageObject {

	public String crearQuery(String doc) {
		String sql = "SELECT * FROM GLIBRA.TMPCNAME WHERE CNNOSS = ‘<documento>’";
		return sql.replace("<documento>", doc);
	}

	public ResultSet ejecutarQuery(String sql) {
		try {
			Sql_Execute sqlExec = new Sql_Execute();
			return sqlExec.sql_Execute(sql);
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}

		return null;
	}

	public void verificarQuery(ResultSet rs, List<List<String>> data) {
		try {
			while (rs.next()) {
				String documentoRecibido = rs.getString(1);
				String documentoEsperado = data.get(0).get(0);
				assertThat(documentoRecibido, equalTo(documentoEsperado));

				String tipoDocRecibido = rs.getString(2);
				String tipoDocEsperado = data.get(0).get(1);
				assertThat(tipoDocRecibido, equalTo(tipoDocEsperado));

				String nombreRecibido = rs.getString(3);
				String nombreEsperado = data.get(0).get(2);
				assertThat(nombreRecibido, equalTo(nombreEsperado));

				String ctrlTerceroRecibido = rs.getString(4);
				String ctrlTerceroEsperado = data.get(0).get(3);
				assertThat(ctrlTerceroRecibido, equalTo(ctrlTerceroEsperado));
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
}
