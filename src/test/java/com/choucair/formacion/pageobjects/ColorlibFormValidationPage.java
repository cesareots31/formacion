package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ColorlibFormValidationPage extends PageObject {

	@FindBy(xpath = "//input[@id='req']")
	public WebElementFacade txtReq;

	@FindBy(xpath = "//*[@id='sport']")
	public WebElementFacade cboSport;

	@FindBy(xpath = "//*[@id='sport2']")
	public WebElementFacade mulSport;

	@FindBy(xpath = "//input[@id='url1']")
	public WebElementFacade txtUrl;

	@FindBy(xpath = "//input[@id='email1']")
	public WebElementFacade txtEmail;

	@FindBy(xpath = "//input[@id='pass1']")
	public WebElementFacade txtPass1;

	@FindBy(xpath = "//input[@id='pass2']")
	public WebElementFacade txtPass2;

	@FindBy(xpath = "//input[@id='minsize1']")
	public WebElementFacade txtMin;

	@FindBy(xpath = "//input[@id='maxsize1']")
	public WebElementFacade txtMax;

	@FindBy(xpath = "//input[@id='number2']")
	public WebElementFacade txtNum;

	@FindBy(xpath = "//input[@id='ip']")
	public WebElementFacade txtIp;

	@FindBy(xpath = "//input[@id='date3']")
	public WebElementFacade txtDate;

	@FindBy(xpath = "//input[@id='past']")
	public WebElementFacade txtDateEar;

	@FindBy(xpath = "//input[@value='Validate']")
	public List<WebElementFacade> btnValidate;

	@FindBy(xpath = "//div[@class='formErrorContent']")
	public WebElementFacade globoInformativo;

	public void required(String dato) {
		txtReq.click();
		txtReq.clear();
		txtReq.sendKeys(dato);
	}

	public void sport(String dato) {
		cboSport.click();
		cboSport.selectByVisibleText(dato);
	}

	public void multipleSelect(String dato) {
		mulSport.selectByVisibleText(dato);
	}

	public void url(String dato) {
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(dato);
	}

	public void email(String dato) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(dato);
	}

	public void password1(String dato) {
		txtPass1.click();
		txtPass1.clear();
		txtPass1.sendKeys(dato);
	}

	public void password2(String dato) {
		txtPass2.click();
		txtPass2.clear();
		txtPass2.sendKeys(dato);
	}

	public void minSize(String dato) {
		txtMin.click();
		txtMin.clear();
		txtMin.sendKeys(dato);
	}

	public void maxSize(String dato) {
		txtMax.click();
		txtMax.clear();
		txtMax.sendKeys(dato);
	}

	public void number(String dato) {
		txtNum.click();
		txtNum.clear();
		txtNum.sendKeys(dato);
	}

	public void ip(String dato) {
		txtIp.click();
		txtIp.clear();
		txtIp.sendKeys(dato);
	}

	public void date(String dato) {
		txtDate.click();
		txtDate.clear();
		txtDate.sendKeys(dato);
	}

	public void dateEarlier(String dato) {
		txtDateEar.click();
		txtDateEar.clear();
		txtDateEar.sendKeys(dato);
	}

	public void validate() {
		btnValidate.get(0).click();
	}

	public void formSinError() {
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
	}

	public void formConError() {
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
	}
}
