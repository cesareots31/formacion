package com.choucair.formacion.pageobjects;

//import static org.junit.Assert.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://colorlib.com/polygon/metis/login.html")
public class ColorlibLoginPage extends PageObject {

	@FindBy(xpath = "//input[@type='text' and @placeholder='Username']")
	public WebElementFacade txtUser;

	@FindBy(xpath = "//input[@type='password' and @placeholder='Password']")
	public WebElementFacade txtPass;

	@FindBy(xpath = "//button[@type='submit' and @class='btn btn-lg btn-primary btn-block']")
	public WebElementFacade btnLogin;

	@FindBy(xpath = "//*[@id='bootstrap-admin-template']")
	public WebElementFacade lblHome;

	public void ingresarDatos(String arg1, String arg2) {
		txtUser.sendKeys(arg1);
		txtPass.sendKeys(arg2);
		btnLogin.click();
	}

	public void verificaHome() {
		String expec = "Bootstrap-Admin-Template";
		String str = lblHome.getText();
		assertThat(str, containsString(expec));
	}
}
