package com.choucair.formacion.pageobjects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ColorlibMenuPage extends PageObject {

	@FindBy(xpath = "//*[contains(text(),'Forms')]")
	public WebElementFacade optForms;

	@FindBy(xpath = "(//a[@href='form-validation.html'])[2]")
	public WebElementFacade optFormValidation;

	@FindBy(xpath = "//*[text()='Popup Validation']")
	public WebElementFacade lblValidar;

	@FindBy(xpath = "(//a[@href='form-general.html'])[2]")
	public WebElementFacade optFormGeneral;

	public void formValidation() {
		optForms.click();
		optFormValidation.click();
		String str = lblValidar.getText();
		assertThat(str, containsString("Popup Validation"));
	}
}
