package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.ColorlibLoginPage;
import com.choucair.formacion.pageobjects.ColorlibMenuPage;

import net.thucydides.core.annotations.Step;

public class PopupValidationSteps {

	ColorlibLoginPage colorlibLoginPage;
	ColorlibMenuPage colorlibMenuPage;

	@Step
	public void login(String arg1, String arg2) {
		colorlibLoginPage.open();
		colorlibLoginPage.ingresarDatos(arg1, arg2);
		colorlibLoginPage.verificaHome();
	}

	@Step
	public void formValidation() {
		colorlibMenuPage.formValidation();
	}
}
