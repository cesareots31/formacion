package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.IseriesMyExtraPage;

import net.thucydides.core.annotations.Step;

public class IseriesMyExtraSteps {

	IseriesMyExtraPage iseriesMyExtraPage;

	@Step
	public void abrirExtra(String rutaCalidad) {
		iseriesMyExtraPage.iniciar_Extra(rutaCalidad);
	}

	@Step
	public void autenticarExtra(String strUsuario, String strClave) {
		if (iseriesMyExtraPage.Sess0 != null) {
			iseriesMyExtraPage.Autenticar_Extra(strUsuario, strClave);
		} else {
			System.out.println("No es posible ejecutar no se cuenta con una sesion activa");
		}
	}
}
