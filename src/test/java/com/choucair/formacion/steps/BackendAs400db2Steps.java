package com.choucair.formacion.steps;

import java.sql.ResultSet;
import java.util.List;

import com.choucair.formacion.pageobjects.BackendAs400db2Page;

import net.thucydides.core.annotations.Step;

public class BackendAs400db2Steps {

	BackendAs400db2Page backendAs400db2Page;

	@Step
	public void consultarCNAME(List<List<String>> data) {
		String documento = data.get(0).get(0);
		String sql = backendAs400db2Page.crearQuery(documento);
		ResultSet rs = backendAs400db2Page.ejecutarQuery(sql);
		backendAs400db2Page.verificarQuery(rs, data);
	}
}
