package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.steps.ColorlibFormValidationSteps;
import com.choucair.formacion.steps.PopupValidationSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PopupValidationDefinition {

	@Steps
	PopupValidationSteps popupValidationSteps;
	@Steps
	ColorlibFormValidationSteps colorlibFormValidationSteps;

	@Given("^autentico en colorlib con usuario \"([^\"]*)\" y clave \"([^\"]*)\"$")
	public void autentico_en_colorlib_con_usuario_y_clave(String arg1, String arg2) {
		popupValidationSteps.login(arg1, arg2);
	}

	@Given("^ingreso a la funcionalidad forms validation$")
	public void ingreso_a_la_funcionalidad_forms_validation() {
		popupValidationSteps.formValidation();
	}

	@When("^diligencio formulario popup validation$")
	public void diligencio_formulario_popup_validation(DataTable dtDatosForm) {
		List<List<String>> data = dtDatosForm.raw();

		for (int i = 1; i < data.size(); i++) {
			colorlibFormValidationSteps.diligenciarDatos(data, i);

			try {
				Thread.sleep(5000L);
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
				ex.printStackTrace();
			}
		}
	}

	@Then("^verifico ingreso exitoso$")
	public void verifico_ingreso_exitoso() {
		colorlibFormValidationSteps.verificarFormExitoso();
	}

	@Then("^verificar que se presente globo informativo de validacion$")
	public void verificar_que_se_presente_globo_informativo_de_validacion() {
		colorlibFormValidationSteps.verificarFormError();
	}
}
