package com.choucair.formacion.definition;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import com.choucair.formacion.steps.IseriesMyExtraSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class IseriesMyExtraDefinition {

	private String user;
	private String password;

	@Steps
	IseriesMyExtraSteps iseriesMyExtraSteps;

	@Given("^abrir MyExtra \"([^\"]*)\"$")
	public void abrir_MyExtra(String rutaCalidad) {
		iseriesMyExtraSteps.abrirExtra(rutaCalidad);
	}

	@When("^autenticar MyExtra$")
	public void autenticar_MyExtra() {
		try {
			Properties prop = new Properties();
			prop.load(new FileReader("D:\\workspace-01\\formacion\\dbconfig.properties"));
			this.user = prop.getProperty("db.user");
			this.password = prop.getProperty("db.password");
			iseriesMyExtraSteps.autenticarExtra(user, password);
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
	}
}
