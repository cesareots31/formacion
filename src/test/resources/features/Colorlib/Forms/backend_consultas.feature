@tag
Feature: hola

@CasoFeliz
  Scenario Outline: consultar tabla cliente CNAME y verificar resultados
    Given consultar CNAME
      | <Documento> | <Tipo Docto> | <Nombre> | <Control Terceros> |

    Examples: 
      | Documento       | Tipo Docto | Nombre         | Control Terceros |
      | 000008000003931 |          3 | PERFORMANCE    |                8 |
      | 000008000004576 |          3 | ELIANA MORALES |                1 |

@MYEXTRA
  Scenario: trabajar con el objeto MyExtra
    Given abrir MyExtra "d:\\calidad.edp"
    When autenticar MyExtra
