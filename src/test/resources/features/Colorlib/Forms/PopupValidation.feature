@Regresion
Feature: formulario popup validation
  el usuario debe poder ingresar al formulario, los datos requeridos.
  cada campo del formulario realiza validaciones de obligatoriedad,
  longitud y formato, el sistema debe presentar las validaciones respectivas
  para cada campo a traves de un globo informativo.

  @CasoExitoso
  Scenario: diligenciamiento exitoso del formulario popup validation,
     no se presenta ningun mensaje de validacion.

    Given autentico en colorlib con usuario "demo" y clave "demo"
    And ingreso a la funcionalidad forms validation
    When diligencio formulario popup validation
      | Required | Select | MultipleS1 | MultipleS2 | Url                   | Email           | Password1 | Password2 | MinSize | MaxSize | Number | IP          | Date       | DateEarlier |
      | Valor1   | Golf   | Tennis     | Golf       | http://www.valor1.com | valor1@mail.com | valor1    | valor1    |  123456 |  123456 | -99.99 | 200.200.5.4 | 2018-01-22 | 2012/09/12  |
    Then verifico ingreso exitoso

  @CasoAlterno
  Scenario: diligenciamiento con errores del formulario popup validation,
    se presenta globo informativo indicando error en el diligenciamiento de algunos de los campos

    Given autentico en colorlib con usuario "demo" y clave "demo"
    And ingreso a la funcionalidad forms validation
    When diligencio formulario popup validation
      | Required | Select         | MultipleS1 | MultipleS2 | Url                   | Email           | Password1 | Password2 | MinSize | MaxSize | Number | IP          | Date       | DateEarlier |
      |          | Golf           | Tennis     | Golf       | http://www.valor1.com | valor1@mail.com | valor1    | valor1    |  123456 |  123456 | -99.99 | 200.200.5.4 | 2018-01-22 | 2012/09/12  |
      | Valor1   | Choose a sport | Tennis     | Golf       | http://www.valor1.com | valor1@mail.com | valor1    | valor1    |  123456 |  123456 | -99.99 | 200.200.5.4 | 2018-01-22 | 2012/09/12  |
    Then verificar que se presente globo informativo de validacion
